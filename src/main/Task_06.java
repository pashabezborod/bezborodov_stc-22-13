package src.main;

public class Task_06 {
    public static int findMinimalCount(int[] array) {

        //counts - массив, в котором индекс соответствует (число + 100), а значение - количеству повторений
        //в получаемом массиве.
        int[] counts = new int[201];
        //minCount - количество повторений искомого числа, minValue - его значение
        int minCount = Integer.MAX_VALUE;
        int minValue = 0;

        //Проверяем каждое число на количество повторений, записываем в counts
        for(int temp : array) {
            if(temp == -101) break;
            counts[temp + 100]++;
        }
        //Находим число с минимальным количеством повторений
        for(int i = 0; i < counts.length; i++) {
            if(counts[i] == 0) continue;
            if (counts[i] < minCount) {
                minCount = counts[i];
                minValue = i;
            }
        }
        //Вуаля - вы великолепны!
        return minValue - 100;
    }
}
