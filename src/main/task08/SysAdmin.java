package src.main.task08;

public class SysAdmin extends Worker {

    public SysAdmin(String name, String lastName) {
        super(name, lastName);
        setProfession("системный администратор");
    }

    @Override
    public void goToVacation(int daysOfVacation) {
        System.out.println(getDescription() + "едет в Кубу на " +  daysOfVacation + "  дней.");
    }

    @Override
    public void goToWork() {
        System.out.println(getDescription() + "обеспечивает работу компьютерной техники.");
    }
}
