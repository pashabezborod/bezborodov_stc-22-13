package src.main.task08;

public abstract class Worker {
    private final String name, lastName;
    private String profession;

    public Worker(String name, String lastName) {
        this.name = name;
        this.lastName = lastName;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public abstract void goToWork();

    public abstract void goToVacation(int daysOfVacation);

    public String getDescription() {
        return String.format("%s %s - %s. Он ", name, lastName, profession);
    }
}
