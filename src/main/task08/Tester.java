package src.main.task08;

public class Tester extends Worker {

    public Tester(String name, String lastName) {
        super(name, lastName);
        setProfession("тестировщик");
    }

    @Override
    public void goToVacation(int daysOfVacation) {
        System.out.println(getDescription() + "едет в Турцию на " +  daysOfVacation + "  дней.");
    }

    @Override
    public void goToWork() {
        System.out.println(getDescription() + "ищет баги. Хорошо ищет.");
    }
}
