package src.main.task08;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Worker[] workers = {
                new Programmer("Bill", "Gates"),
                new SysAdmin("Ваня", "Петров"),
                new Tester("James", "Bach"),
                new DevOps("Петя", "Иванов")
        };

        for(Worker worker : workers) {
            worker.goToWork();
        }
        System.out.println("*************************************");
        for (Worker worker : workers) {
            worker.goToVacation(new Random().nextInt(10, 30));
        }
    }
}
