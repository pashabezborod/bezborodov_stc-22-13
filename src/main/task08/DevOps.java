package src.main.task08;

public class DevOps extends Worker {

    public DevOps(String name, String lastName) {
        super(name, lastName);
        setProfession("ДевОпс");
    }

    @Override
    public void goToVacation(int daysOfVacation) {
        System.out.println(getDescription() + "едет к бате на дачу " +  daysOfVacation + "  дней.");
    }

    @Override
    public void goToWork() {
        System.out.println(getDescription() + "доставляет код в продакшн. И не только.");
    }
}
