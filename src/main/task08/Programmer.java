package src.main.task08;

public class Programmer extends Worker {

    public Programmer(String name, String lastName) {
        super(name, lastName);
        setProfession("программист");
    }

    @Override
    public void goToVacation(int daysOfVacation) {
        System.out.println(getDescription() + "едет на Мальдивы на " +  daysOfVacation + "  дней.");
    }

    @Override
    public void goToWork() {
        System.out.println(getDescription() + "разрабатывает приложения.");
    }
}
