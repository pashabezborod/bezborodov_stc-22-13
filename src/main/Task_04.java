package src.main;

class Task_04 {

    public void getMinInt(int...array) {
        if (array == null || array.length == 0) return;
        int temp = array[0];
        for (int number : array) {
            if (number == -1) break;
            temp = Math.min(temp, number);
        }
        System.out.println("Минимальное число " + temp);
    }

    public void countOddEven(int...array) {
        if (array == null || array.length == 0) return;
        int odd = 0, even = 0;
        for (int number : array) {
            if (number == -1) break;
            if (number % 2 == 0) even++;
            else odd++;
        }
        System.out.printf("%d четных чисел и %d нечетных", even, odd);
    }
}