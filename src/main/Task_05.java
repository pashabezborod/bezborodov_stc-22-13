package src.main;

import java.util.Arrays;

public class Task_05 {

    public static int findIndexInArray(int[] array, int number) {
        int result = -1;
        for (int i = 0; i < array.length; i++)
            if (array[i] == number) result = i;
        return result;
    }

    public static void formatArray(int[] array) {
        int[] tempArray = array.clone();
        Arrays.fill(array, 0);
        int index = 0;
        for(int temp : tempArray)
            if(temp != 0) {
                array[index++] = temp;
            }
    }
}
