package src.main.task09;

public class Rectangle extends Figure {

    public Rectangle(int x, int y) {
        super(x, y);
        name = "прямоугольник";
    }

    @Override
    public String getPerimeter() {
        return String.format(FORMAT, name, (x + y) * 2);
    }
}
