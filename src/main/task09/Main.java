package src.main.task09;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        Random random = new Random();
        Figure[] figures = {
                new Circle(0, 1),
                new Rectangle(2, 3),
                new Ellipse(4, 5),
                new Square(6, 7)
        };
        Movable[] movables = {
                new Circle(8, 9),
                new Square(10, 11),
        };

        for(Figure figure : figures) System.out.println(figure.getPerimeter());
        for(Movable movable : movables) movable.move(random.nextInt(), random.nextInt());
    }
}
