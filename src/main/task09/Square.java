package src.main.task09;

public class Square extends Rectangle implements Movable {

    public Square(int x, int y) {
        super(x, y);
        name = "квадрат";
    }

    @Override
    public void move(int x, int y) {
        this.x = x; this.y = y;
        System.out.println(name + " сдвинут!");
    }
}
