package src.main.task09;

public interface Movable {
    void move(int x, int y);
}
