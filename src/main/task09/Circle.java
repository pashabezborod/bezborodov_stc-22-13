package src.main.task09;

public class Circle extends Ellipse implements Movable {

    public Circle(int x, int y) {
        super(x, y);
        name = "круг";
    }

    @Override
    public void move(int x, int y) {
        this.x = x; this.y = y;
        System.out.println(name + " сдвинут!");
    }
}
