package src.main.task09;

public class Ellipse extends Figure {

    public Ellipse(int x, int y) {
        super(x, y);
        this.name = "эллипс";
    }

    @Override
    public String getPerimeter() {
        return String.format(FORMAT, name, x * y * 10);
    }
}
