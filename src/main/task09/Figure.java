package src.main.task09;

public abstract class Figure {

    protected int x, y;
    protected final String FORMAT = "Периметр %sа равен %d";
    protected String name;

    public Figure(int x, int y) {
        this.x = x; this.y = y;
    }

    public abstract String getPerimeter();
}
