package src.main.task07;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Random random = new Random();
        Human[] humans = new Human[random.nextInt(1, 50)];
        for (int i = 0; i < humans.length; i++) {
            humans[i] = new Human("Name" + i, "LastName" + i, random.nextInt(0, 99));
        }
        Arrays.sort(humans, Comparator.comparing(Human::getAge));
        Arrays.stream(humans).forEach(System.out::println);
    }

}
