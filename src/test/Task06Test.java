package src.test;

import org.junit.Before;
import org.junit.Test;
import src.main.Task_06;
import java.util.Random;

//Я пока не особо понимаю как правильно делать Unit тесты. Но стараюсь.
// Буду очень признателен за комментарии или советы.

public class Task06Test {

    long seed = new Random().nextLong();
    Random random = new Random(seed);
    int[] array;

    @Before
    public void setUp() {
        array = new int[random.nextInt(1, 1000000000)];
        for (int i = 0; i < array.length; i++)
            array[0] = random.nextInt(-100, 101);
        System.out.printf("Random seed is %d\nArray length is %d\n", seed, array.length);
    }

    @Test
    public void findMinimalCount() {
        System.out.println("Result is " + Task_06.findMinimalCount(array));
    }
}